import React from "react";
import RangeSelector, {
	Margin,
	Scale,
	MinorTick,
	Label,
	SliderMarker
} from "devextreme-react/range-selector";

export const RangeSelectorBudget = ({ value, onValueChange }) => (
	<RangeSelector
		id="range-selector"
		title="Select Price Range"
		value={value}
		onValueChanged={e => onValueChange(e.value)}>
		<Margin top={50} />
		<Scale
			startValue={15000}
			endValue={250000}
			minorTickInterval={500}
			tickInterval={15000}>
			<MinorTick visible={false} />
			<Label format="currency" />
		</Scale>
		<SliderMarker format="currency" />
	</RangeSelector>
);

export default RangeSelectorBudget;
