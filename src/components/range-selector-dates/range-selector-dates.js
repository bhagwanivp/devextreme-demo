import React from "react";
import RangeSelector, {
	Margin,
	Scale,
	MinorTick,
	Marker,
	Label,
	Behavior,
	SliderMarker
} from "devextreme-react/range-selector";

let startValue = new Date();
let endValue = new Date();
startValue.setDate(startValue.getDate() + 7);
endValue.setDate(endValue.getDate() + 60);

const defaultProps = {
	title: "Select start and end dates (both inclusive)",
	onValueChanged(e) {
		console.log(e.value, " selected");
	}
};
console.log(startValue, endValue);
const RangeSelectorDates = ({
	value,
	onValueChanged = defaultProps.onValueChanged,
	title = defaultProps.title
}) => {
	return (
		<RangeSelector
			title={title}
			value={value}
			onValueChanged={({ value }) => onValueChanged(value)}>
			<Margin top={50} />
			<Scale
				startValue={startValue}
				endValue={endValue}
				minorTickInterval="day"
				tickInterval="week">
				<MinorTick visible={false} />
				<Marker visible={false} />
				<Label format="monthAndDay" />
			</Scale>
			<Behavior callValueChanged="onMovingComplete" />
			<SliderMarker format="monthAndDay" />
		</RangeSelector>
	);
};

export default RangeSelectorDates;
