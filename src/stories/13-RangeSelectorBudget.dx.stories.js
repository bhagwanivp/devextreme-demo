import React, { useState } from "react";
import notify from "devextreme/ui/notify";
import { RangeSelectorBudget, RangeSelectorDates } from "../components";

export default {
	title: "Range Selector",
	component: RangeSelectorBudget
};

export const BudgetSelector = () => {
	const [budget, setBudget] = useState([20000, 50000]);

	const onBudgetChange = ([min, max]) => {
		notify(`new budget is $${min} to $${max}`);
		setBudget([min, max]);
	};

	return (
		<RangeSelectorBudget value={budget} onValueChange={onBudgetChange} />
	);
};

export const LeaveDaysSelector = () => {
	let startDate = new Date();
	let endDate = new Date();
	startDate.setDate(startDate.getDate() + 10);
	endDate.setDate(endDate.getDate() + 20);

	const calculateLeaves = (start, end) => {
		let currentDate = new Date(start);
		let newCount = 0;

		while (currentDate <= end) {
			if (currentDate.getDay() > 0 && currentDate.getDay() < 6) {
				// NOT sunday or saturday
				newCount++;
			}
			currentDate.setDate(currentDate.getDate() + 1);
		}

		return newCount;
	};

	const [leaveDays, setLeaveDays] = useState([startDate, endDate]);
	const [count, setCount] = useState(calculateLeaves(startDate, endDate));

	const onDaysChange = ([start, end]) => {
		setLeaveDays([start, end]);
		setCount(calculateLeaves(start, end));
	};

	return (
		<div>
			<RangeSelectorDates
				value={leaveDays}
				onValueChanged={onDaysChange}
				title="Select Leave start and end dates"
			/>
			<p>Leaves deductible: {count}</p>
		</div>
	);
};
