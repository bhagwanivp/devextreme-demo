export const navigation = [
	{
		text: "Home",
		path: "/home",
		icon: "home"
	},
	{
		text: "Examples",
		icon: "folder",
		items: [
			{
				text: "Profile",
				path: "/profile"
			},
			{
				text: "Display Data",
				path: "/display-data"
			}
		]
	},
	{
		text: "Components",
		icon: "product",
		items: [
			{
				text: "GET image button",
				path: "/customised-components"
			},
			{
				text: "TextBox with prefix / suffix",
				path: "/text-box-v2"
			},
			{
				text: "Dynamic Form",
				path: "/dynamic-form"
			}
		]
	}
];
